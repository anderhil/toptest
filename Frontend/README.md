# Frontend

Frontend was developed using Angular 7.1.2

Project is structured with separate communication services, models, components, etc.
Bootstrap styles were used for the application with some custom tweaks

Using the UI is quite straightforward, on Home screen there are buttons which lead to the data tables,
from where user can edit the data

Showing of data was based on the simple table, as there were no requirements for something more advanced
Actions as links on the table, different users can access different actions 

## Backend

ASP.NET Core 2.1 using EF Core for working with data
SQL Server as database
JWT Tokens for AUTH
ASP.NET Roles for roles

API is quite simple, I implemented it on go, I have in mind some refactoring I would do, but not for the current stage of the project
